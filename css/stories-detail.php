  <section id="section-gallery">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <div class="breadcrumb">
                  <a href="<?php echo base_url('/') ?>">HOME</a> / <a href="<?php echo base_url('/stories') ?>">STORIES</a> / <a href="#">STORIES DETAIL</a>
               </div>
            </div>
         </div>
      </div>
      <div class="container gallery-container1">
         <div class="row">
         <div class="col-md-9">
         <div class="row">
             <div class="col-md-12">
               <div class="gallery-thumb1 article-div stories-container">
                     <div class="img-gal-container2">
                        <div class="stories-heading">
                           <?php echo $story->post_title; ?>
                        </div>
                        <img src="<?php echo base_url().'asset_admin/assets/uploads/story/original/'.$story->post_thumbnail; ?>" class="gal-img2">
                        <div class="date-stories2"><?php echo date('l, d F Y', strtotime($story->post_publish_date)); ?>
                            <?php if($story->post_image_source != null): ?>
                                <span class="source-photo">(Source Photo : <?php echo $story->post_image_source ?>)</span>
                            <?php endif; ?>
                        </div>
                     </div>
                        <div class="article-txt">
                           <?php echo $story->post_content ?>
                           <span class="author">Author : <?php echo $author; ?></span>
                        </div>
                        <div class="footer2-soc-a mt-20">
                            <div class="share-txt">Share</div>
                            <a class="share-article" href="javascript:void(0)" onclick="window.open('http://www.facebook.com/sharer/sharer.php?u=<?php echo base_url().'news/'.$story->post_slug; ?>&title=<?php echo $metadata['title_share']; ?>&picture=<?php echo $metadata['image_filename']; ?>', 'facebook', 'width=600, height=400')"><i class="fa fa-facebook"></i></a>
                            <a class="share-article" href="javascript:void(0)" onclick="window.open('http://twitter.com/intent/tweet?text=<?php echo $metadata['title_share']; ?>&url=<?php echo base_url().'news/'.$story->post_slug; ?>', 'twitter', 'width=600, height=400')"><i class="fa fa-twitter"></i></a>
                        </div>
                  </div>
            </div>
         </div>
         </div>
          <?php if($related_story != null): ?>
         <div class="col-md-3 fold-news2">
          <div class="card card-sidebar">
            <div class="card-header">RELATED NEWS</div>
            <div class="card-body">
              <div class="row">
                <div class="col-lg-12">
                 
                     <?php foreach($related_story as $rsvalue): ?>

                        <div class="gallery-thumb1 thumb-sidebar">
                           <a href="<?php echo site_url('/story-detail/'.$rsvalue->post_slug) ?>">
                              <div class="img-gal-container">
                                 <img src="<?php echo base_url().'asset_admin/assets/uploads/story/medium/'.$rsvalue->post_thumbnail; ?>" class="gal-img3">
                                 <div class="overlay"></div>
                                 <div class="gal-txt1">
                                    <p class="band-stories"><?php echo $rsvalue->band_name ?></p>
                                    <p><?php echo $rsvalue->post_title ?></p>
                                 </div>
                              </div>
                           </a>
                        </div>
                     <?php endforeach; ?>
                </div>
              </div>
            </div>
          </div>
      </div>
        <?php endif; ?>
      </div>
      </div>