 <section id="slider-main">
         <div id="rev_slider_20_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="invasion" data-source="gallery" style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
            <!-- START REVOLUTION SLIDER 5.4.7.2 auto mode -->
            <div id="rev_slider_20_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.7.2">
               <ul>
                  <!-- SLIDE  -->
                  <li data-index="rs-80" data-transition="slideup" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                     <!-- MAIN IMAGE -->
                     <img src="<?php echo base_url('/asset/inv18/') ?>img/hero2.png"  alt="" title="hero"  width="1920" height="1800" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
                     <!-- LAYERS -->
                     <!-- LAYER NR. 1 -->
                     <div class="tp-caption   tp-resizeme text-slider" 
                        id="slide-80-layer-1" 
                        data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                        data-y="['top','middle','middle','middle']" data-voffset="['1100','164','164','200']" 
                        data-fontsize="['105','50','40','26']"
                        data-lineheight="['105','55','45','35']"
                        data-width="['1200','800','600','300']"
                        data-height="none"
                        data-whitespace="normal"
                        data-type="text" 
                        data-responsive_offset="on" 
                        data-frames='[{"delay":1000,"speed":1000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                        data-textAlign="['center','center','center','center']"
                        data-paddingtop="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        style="z-index: 5; white-space: nowrap; font-size: 70px; line-height: 75px; font-weight: 400; color: #ffffff; letter-spacing: 2px;font-family:geizer;">FOLLOW THE INVASION,<br>JOIN THE CHALLENGE AND WIN EXCLUSIVE LIMITED PRIZES!</div>
                  </li>
               </ul>
               <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
            </div>
         </div>
         <!-- END REVOLUTION SLIDER -->
      </section>
      <section id="section2">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="text-sect2 text-center wow fadeInUp">
                     Witness the Deadsquad and Burgerkill’s journey to invade Europe in<br>SuperInvasion 2018!
                     <div class="button-div">
                        <a href="#video-modal2" data-toggle="modal" class="big btn-vid"><i class="fa fa-play-circle-o"></i> Watch The Teaser</a>
                     </div>
                  </div>
                  <div id="video-modal2" class="modal fade">
                     <div class="modal-dialog dialog-vid">
                       <div class="modal-content modal-vid">
                         <div class="modal-header modal-vid-header">
                             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                             <h4 class="modal-title vid-title">TEASER</h4>
                         </div>
                         <div class="modal-body">
                             <iframe id="video1" width="100%" height="100%" src="https://www.youtube.com/embed/Tx9N-q7f0jk"></iframe>
                         </div>
                     </div>
                   </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section id="section3">
         <div class="container container-band-home">
            <div class="row">
               <div class="col-md-6 text-center band-home">
                  <img src="<?php echo base_url('/asset/inv18/') ?>img/logo-ds.png" class="img-fluid wow fadeInUp">
                  <div class="text-odyssey">
                     Europe Odyssey Tour 2018
                  </div>
                  <img src="<?php echo base_url('/asset/inv18/') ?>img/ds-foto2.png" class="img-fluid wow fadeInLeft">
               <div class="button-div-home">
                  <a href="<?php echo base_url('/journey') ?>" class="btn-prize">
                  SEE JOURNEY
                  </a>
               </div>
               </div>
               <div class="col-md-6 text-center band-home">
                  <img src="<?php echo base_url('/asset/inv18/') ?>img/logo-bk.png" class="img-fluid wow fadeInUp">
                  <div class="text-odyssey">
                     Adamantine European Tour 2018
                  </div>
                  <img src="<?php echo base_url('/asset/inv18/') ?>img/bk-foto2.png" class="img-fluid wow fadeInRight">
                                 <div class="button-div-home">
                  <a href="journey.html" class="btn-prize">
                  SEE JOURNEY
                  </a>
               </div>
               </div>
            </div>
         </div>
      </section>
      <section id="section4">
         <div class="container">
            <div class="row">
               <div id="challenge-sect" class="col-md-12 text-center">
                  <img src="<?php echo base_url('/asset/inv18/') ?>img/challenge-title.png" class="img-fluid wow fadeInUp">
                   <div class="text-challenge2">
                     We Want You To Join!
                  </div>
               </div>
               <div class="col-md-3 text-center ch-block">
                  <a href="#" data-toggle="modal" data-target="#tag"><img src="<?php echo base_url('/asset/inv18/') ?>img/1.png" class="img-fluid wow bounceIn icon-act"></a>
                  <div class="button-div">
                     <a href="#" class="btn-blk" data-toggle="modal" data-target="#tag">TAG IT</a>
                  </div>
               </div>
               <div class="col-md-3 text-center ch-block">
                  <a href="#" data-toggle="modal" data-target="#shout"><img src="<?php echo base_url('/asset/inv18/') ?>img/2.png" class="img-fluid wow bounceIn icon-act"></a>
                  <div class="button-div">
                     <a href="#" class="btn-blk" data-toggle="modal" data-target="#shout">SHOUT IT</a>
                  </div>
               </div>
               <div class="col-md-3 text-center ch-block">
                  <a href="#" data-toggle="modal" data-target="#show"><img src="<?php echo base_url('/asset/inv18/') ?>img/3.png" class="img-fluid wow bounceIn icon-act"></a>
                  <div class="button-div">
                     <a href="#" class="btn-blk" data-toggle="modal" data-target="#show">SHOW IT</a>
                  </div>
               </div>
               <div class="col-md-3 text-center ch-block">
                  <a href="#" data-toggle="modal" data-target="#choose"><img src="<?php echo base_url('/asset/inv18/') ?>img/4.png" class="img-fluid wow bounceIn icon-act"></a>
                  <div class="button-div">
                     <a href="#" class="btn-blk" data-toggle="modal" data-target="#choose">CHOOSE IT</a>
                  </div>
               </div>
            </div>
         </div>
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                 <div class="text-challenge">
                     Jawab semua tantangan dan jadilah bagian dari panasnya keseruan SuperInvasion 2018!
                  </div>
               </div>
            </div>
         </div>
<!--          <div class="container text-center">
            <div class="row">
               <div class="col-md-12">
                  <img src="<?php echo base_url('/asset/inv18/') ?>img/prize.png" class="img-fluid img-prize wow bounceIn">
               </div>
               <div class="button-div">
                  <a href="#" class="btn-prize">
                  DETAIL
                  </a>
               </div>
            </div>
         </div> -->
      </section>
      <section id="section5">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="gallery-title-home">Gallery</div>
               </div>
            </div>
         </div>

         <div class="container">
            <div class="row mobile-gallery">
               <div class="col-md-1 num-gallery num2">
                  <a id="btn-2017">
                     <div class="btn-slide-gallery inactive-gal"><span class="num-ver">7</span><br><span class="num-ver">1</span><br><span class="num-ver">0</span><br><span class="num-ver">2</span></div>
                  </a>
               </div>
               <div class="col-md-1 num-gallery num2">
                  <a id="btn-2018">
                     <div class="btn-slide-gallery"><span class="num-ver">8</span><br><span class="num-ver">1</span><br><span class="num-ver">0</span><br><span class="num-ver">2</span></div>
                  </a>
               </div>
            </div>
         </div>
         <div class="container-fluid slide-2018">
            <div class="row">

               <div class="desktop-gallery">
                  <div class="col-md-1 num-gallery">
                     <a id="btn-20172">
                        <div class="btn-slide-gallery inactive-gal"><span class="num-ver">7</span><br><span class="num-ver">1</span><br><span class="num-ver">0</span><br><span class="num-ver">2</span></div>
                     </a>
                  </div>
                  <div class="col-md-1 num-gallery">
                     <a id="btn-20182">
                        <div class="btn-slide-gallery"><span class="num-ver">8</span><br><span class="num-ver">1</span><br><span class="num-ver">0</span><br><span class="num-ver">2</span></div>
                     </a>
                  </div>
               </div>

               <div id="gal2018" class="col-md-10 num-gallery2">
                  <div class="slide-gallery">
                     <div class="container-fluid">
                        <div class="row">
                           <div class="owl-carousel owl-one owl-theme">

                              <?php if(isset($gallery['2018'])): ?>
                                 <?php foreach($gallery['2018'] as $gal17):?>
                                    <?php if($gal17->post_slug != null): ?>
                                      <div class="item item-slide">
                                         <div class="list-side1">
                                            <img src="<?php echo base_url().'asset_admin/assets/uploads/photo/original/'.$gal17->post_thumbnail; ?>" class="img-gallery-slide">
                                            <a href="<?php echo base_url('/invasion/photo-detail/'.$gal17->post_slug) ?>"><div class="overlay"><div class="text"><i class="fa fa-search"></i></div></div></a>
                                         </div>
                                      </div>
                                  <?php endif; ?>

                                 <?php endforeach;?>
                              <?php endif; ?>
                               
                           
                           </div>
                        </div>
                     </div>
                  </div>
                  <?php if(isset($gallery['2018'])): ?>
                    <div class="coming-soon-gal">COMING SOON</div>
                  <?php endif; ?>
               </div>

               <div id="gal2017" class="col-md-10 num-gallery2">
                  <div class="slide-gallery">
                     <div class="container-fluid">
                        <div class="row">
                           <div class="owl-carousel owl-one owl-theme">

                              <?php if(isset($gallery['2017'])): ?>
                                 <?php foreach($gallery['2017'] as $gal17):?>
                                    <?php if($gal17->post_slug != null): ?>
                                      <div class="item item-slide">
                                         <div class="list-side1">
                                            <img src="<?php echo base_url().'asset_admin/assets/uploads/photo/original/'.$gal17->post_thumbnail; ?>" class="img-gallery-slide">
                                            <a href="<?php echo base_url('/invasion/photo-detail/'.$gal17->post_slug) ?>"><div class="overlay"><div class="text"><i class="fa fa-search"></i></div></div></a>
                                         </div>
                                      </div>
                                    <?php endif; ?>

                                 <?php endforeach;?>
                              <?php endif; ?>

                           </div>
                        </div>
                     </div>
                  </div>
                  <?php if(isset($gallery['2017'])): ?>
                  <?php else: ?>
                    <div class="coming-soon-gal">COMING SOON</div>
                  <?php endif; ?>
               </div>

            </div>
         </div>
      </section>
      <!-- modal -->
      <!-- Modal -->
<div id="tag" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">TAG IT</h4>
      </div>
      <div class="modal-body">
        <p>Follow akun Instagram @supermusic_ID dan @deadsquad.official, tag 4 teman kamu, jawab pertanyaan dan gunakan hashtag #SuperInvasionChallenge.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn-close-modal" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div id="shout" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">SHOUT IT</h4>
      </div>
      <div class="modal-body">
        <p>Upload video dukungan kamu ke DeadSquad berdurasi minimal 15 detik, mention @supermusic_ID dan @deadsquad.official dengan hashtag #SuperInvasionChallenge.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn-close-modal" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div id="show" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">SHOW IT</h4>
      </div>
      <div class="modal-body">
        <p>Pamer kaus merchandise DeadSquad kebanggaan kamu, mention @supermusic_ID dan @deadsquad.official dengan hashtag #SuperInvasionChallenge.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn-close-modal" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div id="choose" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">CHOOSE IT</h4>
      </div>
      <div class="modal-body">
        <p>Pilih kaus hadiah merchandise SuperInvasion di kolom reply Instagram @SuperMusic_ID dan tuliskan hashtag #SuperInvasionChallenge.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn-close-modal" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>