<section id="section-band-profile">
         <div class="cd-timeline js-cd-timeline">
            <div class="cd-timeline__container2">
               <div class="heading-band-profile cd-timeline__img--picture2 js-cd-img2">
                  <div class="number-timeline3">Band Profile</div>
               </div>
               <div class="cd-timeline__img2 cd-timeline__img--picture2 js-cd-img2">
                  <div class="number-timeline2">2018</div>
               </div>
               <div class="cd-timeline__block2 js-cd-block2">
                  <?php $deadsquad = $this->m_frontend2018->get_detail_band_by_id(5); ?>
                  <!-- cd-timeline__img -->
                  <div class="container">
                     <div class="row">
                        <div class="col-md-8">
                           <div class="txt-profile">
                           <?php echo $deadsquad->band_name; ?>
                           </div>
                        </div>
                        <div class="col-md-3 text-center">
                           <img src="<?php echo base_url('asset_admin/assets/uploads/band/medium/').$deadsquad->band_photo; ?>" class="img-band-profile">
                           <div class="overlay">                                 <div class="div-profile-btn">
                                    <img src="<?php echo $deadsquad->band_logo; ?>" class="logo-profile">
                                       <div class="btn-profile">
                                          <a href="<?php echo site_url('/band-profile/').$deadsquad->band_slug ?>">See Details</a>
                                       </div>
                                 </div></div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="cd-timeline__block2 js-cd-block2">
                  <?php $burgerkill = $this->m_frontend2018->get_detail_band_by_id(6); ?>
                  <!-- cd-timeline__img -->
                  <div class="container">
                     <div class="row">
                        <div class="col-md-8">
                           <div class="txt-profile">
                           <?php echo $burgerkill->band_name; ?>
                           </div>
                        </div>
                        <div class="col-md-3 text-center">
                           <img src="<?php echo base_url('asset_admin/assets/uploads/band/medium/').$burgerkill->band_photo; ?>" class="img-band-profile">
                           <div class="overlay">                                 <div class="div-profile-btn">
                                    <img src="<?php echo $burgerkill->band_logo ?>" class="logo-profile">
                                       <div class="btn-profile">
                                          <a href="<?php echo site_url('/band-profile/').$burgerkill->band_slug ?>">See Details</a>
                                       </div>
                                 </div></div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- cd-timeline__block -->
            </div>
            <div class="cd-timeline__container2">
               <div class="cd-timeline__img2 cd-timeline__img--picture2 js-cd-img2">
                  <div class="number-timeline2">2017</div>
               </div>
               <div class="cd-timeline__block2 js-cd-block2">
                  <?php $mooner = $this->m_frontend2018->get_detail_band_by_id(3); ?>
                  <!-- cd-timeline__img -->
                  <div class="container">
                     <div class="row">
                        <div class="col-md-8">
                           <div class="txt-profile">
                           <?php echo $mooner->band_name; ?>
                           </div>
                        </div>
                        <div class="col-md-3 text-center">
                           <img src="<?php echo base_url('asset_admin/assets/uploads/band/medium/').$mooner->band_photo; ?>" class="img-band-profile">
                           <div class="overlay">                                 <div class="div-profile-btn">
                                    <img src="<?php echo $mooner->band_logo ?>" class="logo-profile">
                                       <div class="btn-profile">
                                          <a href="<?php echo site_url('/band-profile/').$mooner->band_slug ?>">See Details</a>
                                       </div>
                                 </div></div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="cd-timeline__block2 js-cd-block2">
                  <?php $kpr = $this->m_frontend2018->get_detail_band_by_id(4); ?>
                  <!-- cd-timeline__img -->
                  <div class="container">
                     <div class="row">
                        <div class="col-md-8">
                           <div class="txt-profile">
                           <?php echo $kpr->band_name; ?>
                           </div>
                        </div>
                        <div class="col-md-3 text-center">
                           <img src="<?php echo base_url('asset_admin/assets/uploads/band/medium/').$kpr->band_photo; ?>" class="img-band-profile">
                           <div class="overlay">                                 <div class="div-profile-btn">
                                    <img src="<?php echo $kpr->band_logo; ?>" class="logo-profile">
                                       <div class="btn-profile">
                                          <a href="<?php echo site_url('/band-profile/').$kpr->band_slug ?>">See Details</a>
                                       </div>
                                 </div></div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="cd-timeline__block2 js-cd-block2">
                  <?php $hydrant = $this->m_frontend2018->get_detail_band_by_id(2); ?>
                  <!-- cd-timeline__img -->
                  <div class="container">
                     <div class="row">
                        <div class="col-md-8">
                           <div class="txt-profile">
                           <?php echo $hydrant->band_name; ?>
                           </div>
                        </div>
                        <div class="col-md-3 text-center">
                           <img src="<?php echo base_url('asset_admin/assets/uploads/band/medium/').$hydrant->band_photo; ?>" class="img-band-profile">
                           <div class="overlay">                                 <div class="div-profile-btn">
                                    <img src="<?php echo $hydrant->band_logo; ?>" class="logo-profile">
                                       <div class="btn-profile">
                                          <a href="<?php echo site_url('/band-profile/').$hydrant->band_slug ?>">See Details</a>
                                       </div>
                                 </div></div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- cd-timeline__block -->
            </div>
            <div class="cd-timeline__container2">
               <div class="cd-timeline__img2 cd-timeline__img--picture2 js-cd-img2">
                  <div class="number-timeline2">2016</div>
               </div>
               <div class="cd-timeline__block2 js-cd-block2">
                  <?php $sigit = $this->m_frontend2018->get_detail_band_by_id(1); ?>
                  <!-- cd-timeline__img -->
                  <div class="container">
                     <div class="row">
                        <div class="col-md-8">
                           <div class="txt-profile">
                           <?php echo $sigit->band_name; ?>
                           </div>
                        </div>
                        <div class="col-md-3 text-center">
                           <img src="<?php echo base_url('asset_admin/assets/uploads/band/medium/').$sigit->band_photo; ?>" class="img-band-profile">
                           <div class="overlay">                                 <div class="div-profile-btn">
                                    <img src="<?php echo $sigit->band_logo ?>" class="logo-profile">
                                       <div class="btn-profile">
                                          <a href="<?php echo site_url('/band-profile/').$sigit->band_slug ?>">See Details</a>
                                       </div>
                                 </div></div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="cd-timeline__block2 js-cd-block2">
                  <!-- cd-timeline__img -->
                   <div class="container">
                     <div class="row">
                        <div class="col-md-8">
                           <div class="txt-profile">
                           <?php echo $hydrant->band_name; ?>
                           </div>
                        </div>
                        <div class="col-md-3 text-center">
                              <img src="<?php echo base_url('asset_admin/assets/uploads/band/medium/').$hydrant->band_photo; ?>" class="img-band-profile">
                              <div class="overlay">
                                 <div class="div-profile-btn">
                                    <img src="<?php echo $hydrant->band_logo; ?>" class="logo-profile">
                                       <div class="btn-profile">
                                          <a href="<?php echo site_url('/band-profile/').$hydrant->band_slug; ?>">See Details</a>
                                       </div>
                                 </div>
                              </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- cd-timeline__block -->
            </div>
         </div>
         <!-- cd-timeline -->
      </section>