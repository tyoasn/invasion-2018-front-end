<section id="section-band-profile">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <div class="breadcrumb">
                  <a href="<?php echo base_url('/') ?>">HOME</a> / <a href="<?php echo base_url('/band-profile') ?>">BAND PROFILE</a> / <a href="#">BAND DETAIL</a>
               </div>
            </div>
         </div>
      </div>
      <div class="container heading-lineup">
         <div class="row">
            <div class="col-md-6">
               <div class="band-text">
                  <div class="heading-band-detail wow fadeInUp"><?php echo $band->band_name ?></div>
                     <?php echo $band->band_description ?>
               </div>
            </div>
            <div class="col-md-6">
               <div class="band-photo">
                  <img src="<?php echo base_url().'asset_admin/assets/uploads/band/original/'.$band->band_photo; ?>" class="img-responsive">
                  <ul class="ul-profile">
                     <li class="li-profile"><a class="socmed-footer socmed-profile" href="<?php echo $band->band_facebook ?>" title="Facebook" target="_blank"><i class="fa fa-facebook"></i></a></li>
                     <li class="li-profile"><a class="socmed-footer socmed-profile" href="<?php echo $band->band_twitter ?>" title="Twitter" target="_blank"><i class="fa fa-twitter"></i></a></li>
                     <li class="li-profile"><a class="socmed-footer socmed-profile" href="<?php echo $band->band_instagram ?>" title="Instagram" target="_blank"><i class="fa fa-instagram"></i></a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <?php if($band->band_video_url != ""): ?>
      <div class="container gallery-container1">
         <div class="row">
            <div class="gallery-video">
                  <div class="col-md-12">
                     <a href="#video-modal1" data-toggle="modal" class="big"><img class="video-thumb" src="<?php echo base_url('/asset_admin/assets/uploads/band_video/large/').$band->band_video_photo; ?>" alt="" title="<?php echo $band->band_name; ?>" /><span class="play-vid"><i class="fa fa-play"></i></span></a>
                  </div>  
            </div>
            <div id="video-modal1" class="modal fade">
            <div class="modal-dialog dialog-vid">
              <div class="modal-content modal-vid">
                  <div class="modal-header modal-vid-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title vid-title">Video</h4>
                  </div>
                  <div class="modal-body">
                    <iframe id="video1" width="100%" height="100%" src="https://www.youtube.com/embed/<?php echo $band->band_video_url; ?>"></iframe>
                  </div>
            </div>
          </div>
        </div>
      </div>
      </div>
    <?php endif; ?>
   </section>