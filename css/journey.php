    <section id="section-journey">
         <div id="slider-about">
            <div id="rev_slider_24_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="journey-header" data-source="gallery" style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
               <!-- START REVOLUTION SLIDER 5.4.7.2 auto mode -->
               <div id="rev_slider_24_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.7.2">
                  <ul>
                     <!-- SLIDE  -->
                     <li data-index="rs-98" data-transition="slidedown" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="<?php echo base_url('asset/inv18/') ?>img/journey/header-journey.png"  alt="" title="header-journey"  width="1920" height="739" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
                        <!-- LAYERS -->
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption   tp-resizeme" 
                           id="slide-98-layer-1" 
                           data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                           data-y="['middle','middle','middle','middle']" data-voffset="['-300','-200','-250','-180']"
                           data-fontsize="['120','50','40','50']"
                           data-width="none"
                           data-height="none"
                           data-whitespace="nowrap"
                           data-type="text" 
                           data-responsive_offset="on" 
                           data-frames='[{"delay":0,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                           data-textAlign="['inherit','inherit','inherit','inherit']"
                           data-paddingtop="[0,0,0,0]"
                           data-paddingright="[0,0,0,0]"
                           data-paddingbottom="[0,0,0,0]"
                           data-paddingleft="[0,0,0,0]"
                           style="z-index: 5; white-space: nowrap; font-size: 120px; line-height: 22px; font-weight: 400; color: #000000; letter-spacing: 0px;font-family:geizer;">Journey To The Glory </div>
                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption   tp-resizeme" 
                           id="slide-98-layer-2" 
                           data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                           data-y="['top','top','top','top']" data-voffset="['300','282','282','120']" 
                           data-width="['850','600','450','280']"
                           data-fontsize="['40','30','30','26']"
                           data-height="none"
                           data-whitespace="normal"
                           data-type="text" 
                           data-responsive_offset="on" 
                           data-frames='[{"delay":2030,"speed":1000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                           data-textAlign="['center','center','center','center']"
                           data-paddingtop="[0,0,0,0]"
                           data-paddingright="[0,0,0,0]"
                           data-paddingbottom="[0,0,0,0]"
                           data-paddingleft="[0,0,0,0]"
                           style="z-index: 6; min-width: 900px; max-width: 900px; white-space: nowrap; font-size: 40px; line-height: 48px; font-weight: 800; color: #000000; letter-spacing: 0px;font-family:Roboto Condensed;">Ikuti setiap langkah DeadSquad dan Burgerkill menginvasi benua Eropa selama bulan Oktober 2018!</div>
                     </li>
                  </ul>
                  <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
               </div>
            </div>
            <!-- END REVOLUTION SLIDER -->
         </div>
         <div class="container">
            <div class="row">
               <div id="tabs">
                  <ul class="nav nav-tabs nav-justified nav-tab-journey">
                     <li <?php if(isset($year) && $year == '2016') { echo 'class="active"'; } ?>><a class="nav-item nav-journey nav-3" href="<?php echo base_url('journey?year=2016') ?>">2016</a></li>
                     <li <?php if(isset($year) && $year == '2017') { echo 'class="active"'; } ?>><a class="nav-item nav-journey nav-2" href="<?php echo base_url('journey?year=2017') ?>">2017</a></li>
                     <li <?php if(isset($year) && $year == '2018') { echo 'class="active"'; } ?>><a class="nav-item nav-journey nav-1" href="<?php echo base_url('journey?year=2018') ?>">2018</a></li>
                  </ul>
               </div>
            </div>
         </div>
         <div class="container">
            <div class="row">
               <div class="col-md-5 text-center">
                  <img src="<?php echo base_url('asset/inv18/') ?>img/logo-ds.png" class="wow bounceIn">
               </div>
               <div class="col-md-2 text-center">
                  <div class="collab-icon">+</div>
               </div>
               <div class="col-md-5 text-center">
                  <img src="<?php echo base_url('asset/inv18/') ?>img/logo-bk.png" class="wow bounceIn">
               </div>
            </div>
         </div>

         <?php if($journey != FALSE): ?>

            <?php foreach($journey as $bandkey => $bandvalue): ?>
               <div class="container">
                  <div class="row">
                     <div class="col-md-12 text-center">
                        <!-- <img src="<?php echo base_url('asset/inv18/') ?>img/logo-ds.png" class="wow bounceIn"> -->
                        <!-- <div class="place-timeline"><?php echo $bandkey; ?></div> -->
                        <?php echo $bandkey; ?>
                     </div>
                  </div>
               </div>

               <div class="cd-timeline js-cd-timeline">
                  <div class="cd-timeline__container">
                     <?php $i = 1; ?>

                     <?php foreach($bandvalue as $key => $data): ?>                  

                        <?php if($i < 10): ?>
                           <?php $j = '0'.$i++; ?>
                        <?php else: ?>
                           <?php $j = $i++; ?>
                        <?php endif; ?>

                        <?php if($data->post_type === 'stories'): ?>

                           <?php if($data->post_random_type == 1): ?>

                              <!-- story tipe 1 -->
                              <div class="cd-timeline__block js-cd-block" id="<?php echo $key ?>">
                                 <div class="cd-timeline__img cd-timeline__img--picture js-cd-img">
                                    <div class="number-timeline"><?php echo $j; ?></div>
                                 </div>
                                 <!-- cd-timeline__img -->
                                 <div class="cd-timeline__content js-cd-content">
                                    <div class="content-timeline wow fadeInRight">
                                       <img src="<?php echo base_url('asset_admin/assets/uploads/story/original/'.$data->post_thumbnail) ?>" class="img-timeline1">
                                       <div class="div-btn-timeline">
                                          <a href="<?php echo $data->post_slug ?>" class="btn-timeline">Read Their Story Here <i class="fa fa-long-arrow-right"></i></a>
                                       </div>
                                    </div>
                                    <span class="cd-timeline__date wow fadeInLeft">
                                       <div class="date-timeline"><?php echo $data->post_publish_date ?></div>
                                       <div class="place-timeline"><?php echo $data->post_title ?></div>
                                       <div class="nation-timeline"><?php echo $data->post_country ?>, <?php echo $data->post_city ?></div>
                                    </span>
                                 </div>
                              </div>

                           <?php elseif($data->post_random_type == 2): ?>

                              <!-- story tipe 2 -->
                              <!-- cd-timeline__block -->
                              <div class="cd-timeline__block js-cd-block" id="<?php echo $key ?>">
                                 <div class="cd-timeline__img cd-timeline__img--picture js-cd-img">
                                    <div class="number-timeline"><?php echo $j; ?></div>
                                 </div>
                                 <!-- cd-timeline__img -->
                                 <div class="cd-timeline__content js-cd-content">
                                    <div class="content-timeline wow fadeInRight">
                                       <img src="<?php echo base_url('asset_admin/assets/uploads/story/original/'.$data->post_thumbnail) ?>" class="img-timeline1-1">
                                       <div class="timeline-vid-txt1">
                                          <span class="txt-hollow"><?php echo $data->post_band_name ?> Say Hello</span> <?php echo $data->post_city ?> <?php echo $data->post_country ?>
                                       </div>
                                       <p class="txt-stories">
                                          <?php echo $data->post_album_content_desc; ?> 
                                       </p>
                                       <div class="div-btn-timeline">
                                          <a href="<?php echo $data->post_slug ?>" class="btn-timeline">Read The Complete Story <i class="fa fa-long-arrow-right"></i></a>
                                       </div>
                                    </div>
                                    <span class="cd-timeline__date wow fadeInLeft">
                                       <div class="date-timeline"><?php echo $data->post_publish_date ?></div>
                                       <div class="place-timeline"><?php echo $data->post_title ?></div>
                                       <div class="nation-timeline"><?php echo $data->post_country ?>, <?php echo $data->post_city ?></div>
                                    </span>
                                 </div>
                              </div>

                           <?php endif; ?>

                        <?php elseif($data->post_type === 'album'): ?>

                           <!-- gallery tipe -->
                           <!-- cd-timeline__block -->
                           <div class="cd-timeline__block js-cd-block" id="<?php echo $key ?>">
                              <div class="cd-timeline__img cd-timeline__img--picture js-cd-img">
                                 <div class="number-timeline"><?php echo $j; ?></div>
                              </div>
                              <!-- cd-timeline__img -->
                              <div class="cd-timeline__content js-cd-content">
                                 <div class="content-timeline wow fadeInRight">  

                                    <?php if(isset($data->post_album_content[0])): ?>
                                       <img src="<?php echo base_url('asset_admin/assets/uploads/photo/original/'.$data->post_album_content[0]) ?>" class="img-timeline2-1">
                                    <?php endif; ?>
                                    <?php if(isset($data->post_album_content[1])): ?>
                                       <img src="<?php echo base_url('asset_admin/assets/uploads/photo/original/'.$data->post_album_content[1]) ?>" class="img-timeline3-1">
                                    <?php endif; ?>
                                    <?php if(isset($data->post_album_content[2])): ?>
                                       <img src="<?php echo base_url('asset_admin/assets/uploads/photo/original/'.$data->post_album_content[2]) ?>" class="img-timeline3-1">
                                    <?php endif; ?>
                                    <div class="div-btn-timeline">
                                       <a href="<?php echo $data->post_slug ?>" class="btn-timeline">Check Their Photos Here <i class="fa fa-long-arrow-right"></i></a>
                                    </div>
                                 </div>
                                 <span class="cd-timeline__date wow fadeInLeft">
                                    <div class="date-timeline"><?php echo $data->post_publish_date ?></div>
                                    <div class="place-timeline"><?php echo $data->post_title ?></div>
                                    <div class="nation-timeline"><?php echo $data->post_country ?>, <?php echo $data->post_city ?></div>
                                 </span>
                              </div>
                           </div>

                        <?php elseif($data->post_type === 'video'): ?>

                              <!-- video tipe -->
                           <!-- cd-timeline__block -->
                           <div class="cd-timeline__block js-cd-block" id="<?php echo $key ?>">
                              <div class="cd-timeline__img cd-timeline__img--picture js-cd-img">
                                 <div class="number-timeline"><?php echo $j; ?></div>
                              </div>
                              <!-- cd-timeline__img -->
                              <div class="cd-timeline__content js-cd-content">
                                 <div class="content-timeline wow fadeInRight">
                                    <div class="timeline-vid-txt">
                                       <span class="txt-hollow">SWINGIN' AT</span> <?php echo $data->post_country ?>
                                    </div>
                                    <img src="<?php echo base_url('asset_admin/assets/uploads/video/original/'.$data->post_thumbnail) ?>" class="img-timeline4">
                                    <div class="div-btn-timeline">
                                       <a href="<?php echo $data->post_slug ?>" class="btn-timeline">Watch Their Performance <i class="fa fa-long-arrow-right"></i></a>
                                    </div>
                                 </div>
                                 <span class="cd-timeline__date wow fadeInLeft">
                                    <div class="date-timeline"><?php echo $data->post_publish_date ?></div>
                                    <div class="place-timeline"><?php echo $data->post_title ?></div>
                                    <div class="nation-timeline"><?php echo $data->post_country ?>, <?php echo $data->post_city ?></div>
                                 </span>
                              </div>
                           </div>

                        <?php endif; ?>

                     <?php endforeach; ?>

                  </div>
               </div>
               <!-- cd-timeline -->
            <?php endforeach; ?>

         <?php else: ?>

         <?php endif; ?>

      </section>
